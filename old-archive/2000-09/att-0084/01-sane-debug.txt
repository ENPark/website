[sanei_debug] Setting debug level of mustek_pp to 128.
[mustek_pp] init: SANE v1.0.3, backend v1.0.9-devel
[mustek_pp] I wouldn't let myself be root if I were you...
[mustek_pp] init: option io-mode 2
[mustek_pp] init: trying port `0x378'
[mustek_pp] attach: device 0x378 attached
[mustek_pp] attach: asic 0xa5, ccd 01
[mustek_pp] attach: use600 is `no'
[mustek_pp] init: naming device 0x378 'SE 1200 ED Plus'
[mustek_pp] init: device 0x378 is from 'Mustek'
[mustek_pp] init: option wait-lamp 10
[mustek_pp] init: setting wait-lamp to 10 for device SE 1200 ED Plus
[mustek_pp] init: device SE 1200 ED Plus is a 600 dpi scanner
[mustek_pp] open: device `'
[mustek_pp] open: trying default device SE 1200 ED Plus
[mustek_pp] open: range (0.0,0.0)-(220.133316,296.333313)
[mustek_pp] init_options(v1.0.9-devel): line 2701: debug exception
[mustek_pp] ASSERT((dev->asic_id == 0xA8) || ((dev->asic_id == 0xA5) && (dev->ccd_type == 0))) failed
[mustek_pp] expect disaster...
[mustek_pp] config_ccd: 600 dpi, mode 2, invert 0, size 10
[mustek_pp] set_dpi_value: value 0x21
[mustek_pp] set_line_adjust: value = 82 (0x0052)
[mustek_pp] set_line_adjust: ppl 10 (0), adjust 82, skip 0
[mustek_pp] motor_controll_1015: control code 0xd3
[mustek_pp] config_ccd: 600 dpi, mode 2, invert 0, size 10
[mustek_pp] set_dpi_value: value 0x21
[mustek_pp] set_line_adjust: value = 82 (0x0052)
[mustek_pp] set_line_adjust: ppl 10 (0), adjust 82, skip 0
[mustek_pp] open: success
[mustek_pp] control_option: option 0, action 0
[mustek_pp] control_option: option 0, action 0
[mustek_pp] get_option_descriptor: requested option 0 ((null))
[mustek_pp] get_option_descriptor: requested option 1 ((null))
[mustek_pp] get_option_descriptor: requested option 2 (mode)
[mustek_pp] get_option_descriptor: requested option 3 (resolution)
[mustek_pp] get_option_descriptor: requested option 4 (preview)
[mustek_pp] get_option_descriptor: requested option 5 (preview-in-gray)
[mustek_pp] get_option_descriptor: requested option 6 ((null))
[mustek_pp] get_option_descriptor: requested option 7 (tl-x)
[mustek_pp] get_option_descriptor: requested option 8 (tl-y)
[mustek_pp] get_option_descriptor: requested option 9 (br-x)
[mustek_pp] control_option: option 9, action 0
[mustek_pp] get_option_descriptor: requested option 10 (br-y)
[mustek_pp] control_option: option 10, action 0
[mustek_pp] get_option_descriptor: requested option 11 ((null))
[mustek_pp] get_option_descriptor: requested option 12 (custom-gamma)
[mustek_pp] get_option_descriptor: requested option 13 (gamma-table)
[mustek_pp] get_option_descriptor: requested option 14 (red-gamma-table)
[mustek_pp] get_option_descriptor: requested option 15 (green-gamma-table)
[mustek_pp] get_option_descriptor: requested option 16 (blue-gamma-table)
[mustek_pp] control_option: option 7, action 0
[mustek_pp] control_option: option 8, action 0
[mustek_pp] control_option: option 7, action 0
[mustek_pp] get_option_descriptor: requested option 9 (br-x)
[mustek_pp] control_option: option 9, action 1
[mustek_pp] control_option: option 8, action 0
[mustek_pp] get_option_descriptor: requested option 10 (br-y)
[mustek_pp] control_option: option 10, action 1
[mustek_pp] get_parameters: mode `Color'
[mustek_pp] get_parameters: 600 dpi
[mustek_pp] get_parameters: resolution 600 dpi -> hardware 600 dpi
[mustek_pp] get_parameters: 5200x7000 pixels
[mustek_pp] start: maybe waiting for lamp...
[mustek_pp] config_ccd: 600 dpi, mode 2, invert 0, size 5200
[mustek_pp] set_dpi_value: value 0x21
[mustek_pp] set_line_adjust: value = 41602 (0xa282)
[mustek_pp] set_line_adjust: ppl 5200 (5200), adjust 41602, skip 0
[mustek_pp] motor_controll_1015: control code 0xd3
[mustek_pp] config_ccd: 600 dpi, mode 2, invert 0, size 5200
[mustek_pp] set_dpi_value: value 0x21
[mustek_pp] set_line_adjust: value = 41602 (0xa282)
[mustek_pp] set_line_adjust: ppl 5200 (5200), adjust 41602, skip 0
[mustek_pp] start: executing calibration
[mustek_pp] calibrate entered (asic = 0xa5)
[mustek_pp] calibrate_device_101x: use600 = yes
[mustek_pp] config_ccd: 600 dpi, mode 2, invert 0, size 10
[mustek_pp] set_dpi_value: value 0x21
[mustek_pp] set_line_adjust: value = 82 (0x0052)
[mustek_pp] set_line_adjust: ppl 10 (5200), adjust 82, skip 0
[mustek_pp] config_ccd: 300 dpi, mode 2, invert 0, size 150
[mustek_pp] set_dpi_value: value 0x51
[mustek_pp] set_line_adjust: value = 1202 (0x04b2)
[mustek_pp] set_line_adjust: ppl 150 (5200), adjust 1202, skip 0
[mustek_pp] motor_controll_1015: control code 0x43
[mustek_pp] motor_controll_1015: control code 0x43
[mustek_pp] motor_controll_1015: control code 0x43
[mustek_pp] motor_controll_1015: control code 0x43
[mustek_pp] config_ccd: 600 dpi, mode 1, invert 0, size 5200
[mustek_pp] set_dpi_value: value 0x21
[mustek_pp] set_line_adjust: value = 5202 (0x1452)
[mustek_pp] set_line_adjust: ppl 5200 (5200), adjust 5202, skip 0
[mustek_pp] motor_controll_1015: control code 0x43

    { the above line repeated 42 times }

[mustek_pp] config_ccd: 600 dpi, mode 2, invert 0, size 5200
[mustek_pp] set_dpi_value: value 0x21
[mustek_pp] set_line_adjust: value = 41602 (0xa282)
[mustek_pp] set_line_adjust: ppl 5200 (5200), adjust 41602, skip 4
[mustek_pp] motor_controll_1015: control code 0x43

    { the above line repeated 102 times }

[mustek_pp] config_ccd: 600 dpi, mode 2, invert 0, size 5200
[mustek_pp] set_dpi_value: value 0x21
[mustek_pp] set_line_adjust: value = 41602 (0xa282)
[mustek_pp] set_line_adjust: ppl 5200 (5200), adjust 41602, skip 4
[mustek_pp] calibrate: ref_black 0, blackpos 106
[mustek_pp] move_motor: 128 steps (forward)
[mustek_pp] motor_controll_1015: control code 0x63

    { the above line repeated 127 times }

[mustek_pp] start: device ready for scanning
[mustek_pp] get_parameters: can't set parameters while scanning
[mustek_pp] sane_mustek_pp_get_parameters(v1.0.9-devel): line 3912: debug exception
scanimage: scanning image of size 5200x7000 pixels at 24 bits/pixel
scanimage: acquiring RGB frame, 8 bits/sample
scanimage: reading one scanline, 15600 bytes...	[mustek_pp] read: scanning next 67 lines
[mustek_pp] motor_controll_1015: control code 0x43

    { the above line repeated 74 times }

[mustek_pp] read: delivering 15600 bytes
PASS
scanimage: reading one byte...		[mustek_pp] read: delivering 1 bytes
PASS
scanimage: stepped read, 2 bytes... 	[mustek_pp] read: delivering 2 bytes
PASS
scanimage: stepped read, 4 bytes... 	[mustek_pp] read: delivering 4 bytes
PASS
scanimage: stepped read, 8 bytes... 	[mustek_pp] read: delivering 8 bytes
PASS
scanimage: stepped read, 16 bytes... 	[mustek_pp] read: delivering 16 bytes
PASS
scanimage: stepped read, 32 bytes... 	[mustek_pp] read: delivering 32 bytes
PASS
scanimage: stepped read, 64 bytes... 	[mustek_pp] read: delivering 64 bytes
PASS
scanimage: stepped read, 128 bytes... 	[mustek_pp] read: delivering 128 bytes
PASS
scanimage: stepped read, 256 bytes... 	[mustek_pp] read: delivering 256 bytes
PASS
scanimage: stepped read, 512 bytes... 	[mustek_pp] read: delivering 512 bytes
PASS
scanimage: stepped read, 1024 bytes... 	[mustek_pp] read: delivering 1024 bytes
PASS
scanimage: stepped read, 2048 bytes... 	[mustek_pp] read: delivering 2048 bytes
PASS
scanimage: stepped read, 4096 bytes... 	[mustek_pp] read: delivering 4096 bytes
PASS
scanimage: stepped read, 8192 bytes... 	[mustek_pp] read: delivering 8192 bytes
PASS
scanimage: stepped read, 16384 bytes... 	[mustek_pp] read: delivering 16384 bytes
PASS
scanimage: stepped read, 16383 bytes... 	[mustek_pp] read: delivering 16383 bytes
PASS
scanimage: stepped read, 8191 bytes... 	[mustek_pp] read: delivering 8191 bytes
PASS
scanimage: stepped read, 4095 bytes... 	[mustek_pp] read: delivering 4095 bytes
PASS
scanimage: stepped read, 2047 bytes... 	[mustek_pp] read: delivering 2047 bytes
PASS
scanimage: stepped read, 1023 bytes... 	[mustek_pp] read: delivering 1023 bytes
PASS
scanimage: stepped read, 511 bytes... 	[mustek_pp] read: delivering 511 bytes
PASS
scanimage: stepped read, 255 bytes... 	[mustek_pp] read: delivering 255 bytes
PASS
scanimage: stepped read, 127 bytes... 	[mustek_pp] read: delivering 127 bytes
PASS
scanimage: stepped read, 63 bytes... 	[mustek_pp] read: delivering 63 bytes
PASS
scanimage: stepped read, 31 bytes... 	[mustek_pp] read: delivering 31 bytes
PASS
scanimage: stepped read, 15 bytes... 	[mustek_pp] read: delivering 15 bytes
PASS
scanimage: stepped read, 7 bytes... 	[mustek_pp] read: delivering 7 bytes
PASS
scanimage: stepped read, 3 bytes... 	[mustek_pp] read: delivering 3 bytes
PASS
[mustek_pp] cancel: stopping current scan
[mustek_pp] config_ccd: 600 dpi, mode 2, invert 0, size 5200
[mustek_pp] set_dpi_value: value 0x21
[mustek_pp] set_line_adjust: value = 41602 (0xa282)
[mustek_pp] set_line_adjust: ppl 5200 (5200), adjust 41602, skip 0
[mustek_pp] motor_controll_1015: control code 0xd3
[mustek_pp] config_ccd: 600 dpi, mode 2, invert 0, size 5200
[mustek_pp] set_dpi_value: value 0x21
[mustek_pp] set_line_adjust: value = 41602 (0xa282)
[mustek_pp] set_line_adjust: ppl 5200 (5200), adjust 41602, skip 4
[mustek_pp] close: maybe waiting for lamp...
[mustek_pp] config_ccd: 600 dpi, mode 2, invert 0, size 5200
[mustek_pp] set_dpi_value: value 0x21
[mustek_pp] set_line_adjust: value = 41602 (0xa282)
[mustek_pp] set_line_adjust: ppl 5200 (5200), adjust 41602, skip 0
[mustek_pp] motor_controll_1015: control code 0xd3
[mustek_pp] config_ccd: 600 dpi, mode 2, invert 0, size 5200
[mustek_pp] set_dpi_value: value 0x21
[mustek_pp] set_line_adjust: value = 41602 (0xa282)
[mustek_pp] set_line_adjust: ppl 5200 (5200), adjust 41602, skip 4
[mustek_pp] close: device closed
[mustek_pp] exit: (...)

